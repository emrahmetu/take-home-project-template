from flask import Flask, request

app = Flask(__name__)


@app.route('/register', methods=['POST'])
def register():
    # request
    ...


@app.route('/login', methods=['POST'])
def login():
    # request
    ...


@app.route('/properties', methods=['GET'])
def properties():
    ...


@app.route('/property', methods=['POST'])
def property():
    # request
    ...


@app.route('/property/create', methods=['POST'])
def property_create():
    # request
    ...


@app.route('/property/update', methods=['POST'])
def property_update():
    # request
    ...


@app.route('/property/delete', methods=['DELETE'])
def property_delete():
    # request
    ...


if __name__ == '__main__':
    app.run()
